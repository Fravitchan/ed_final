#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore_RB.h"

#define RED 1
#define BLACK 0

//Informa��es da tabela csv
typedef struct cadastro_pessoa{
    unsigned int codigo;
    char nome[35];
    unsigned int idade;
    char empresa[35];
    char departamento[35];
    float salario;
}dados; //informa��es cadastrais


// ===== Fun��es RB
struct NO_RB{
    int info;
    struct NO_RB *esq;
    struct NO_RB *dir;
    int cor;
};

arvoreLLRB *cria_arvLLRB(){

    arvoreLLRB *raizRB = (arvoreLLRB*) malloc(sizeof(arvoreLLRB));
    if(raizRB != NULL){
        *raizRB = NULL;
    }
    return raizRB;
}


//Fun��es auxiliares
int cor(struct NO_RB *H){
    if(H == NULL){
        return BLACK;
    }else{
        return H->cor;
    }
}

void trocaCor(struct NO_RB *H){
    H->cor = !H->esq->cor;
    if(H->esq != NULL){
        H->esq->cor = !H->esq->cor;
    }
    if(H->dir != NULL){
        H->dir->cor = !H->dir->cor;
    }
}

struct NO_RB *rotacionaEsquerda(struct NO_RB *A){
    struct NO_RB *B = A->dir;
    A->dir = B->esq;
    B->esq = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NO_RB *rotacionaDireita(struct NO_RB *A){
    struct NO_RB *B = A->esq;
    A->esq = B->dir;
    B->dir = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NO_RB *move2EsqRED(struct NO_RB *H){
    trocaCor(H);
    if(cor(H->dir->esq) == RED){
        H->dir = rotacionaDireita(H->dir);
        H = rotacionaEsquerda(H);
        trocaCor(H);
    }
    return H;
};

struct NO_RB *move2DirRED(struct NO_RB *H){
    trocaCor(H);
    if(cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
        trocaCor(H);
    }
    return H;
};

struct NO_RB *balancear(struct NO_RB *H){
    if(cor(H->dir) == RED){
        H = rotacionaEsquerda(H);
    }
    if(H->esq != NULL && cor(H->dir) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};



struct NO_RB *insereNO(struct NO_RB *H, int valor, int *resp){
    if(H == NULL){
        struct NO_RB *novo;
        novo = (struct NO_RB*) malloc(sizeof(struct NO_RB));
        if(novo == NULL){
            *resp = 0;
            return NULL;
        }
        novo->info = valor;
        novo->cor = RED;
        novo->dir = NULL;
        novo->esq = NULL;
        *resp = 1;
        return novo;
    }
    if(valor == H->info){
        *resp = 0;
    }else{
        if(valor < H->info){
            H->esq = insereNO(H->esq, valor, resp);
        }else{
            H->dir = insereNO(H->dir, valor, resp);
        }
    }
    if(cor(H->dir) == RED && cor(H->esq) == BLACK){
        H = rotacionaEsquerda(H);
    }
    if(cor(H->esq) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};

int insere_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    int resp;
    //fun��o respons�vel pela busca do local de inser��o do n�
    *raizRB = insereNO(*raizRB, valor, &resp);
    if((*raizRB) != NULL){
        (*raizRB)->cor = BLACK;
    }
    return resp;
}


struct NO_RB *removeMenor(struct NO_RB *H){
    if(H->esq == NULL){
        free(H);
        return NULL;
    }
    if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
        H = move2EsqRED(H);
    }
    H->esq = removeMenor(H->esq);
    return balancear(H);
};

struct NO_RB *procuraMenor(struct NO_RB* atual){
    struct NO_RB *no1 = atual;
    struct NO_RB *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
};

struct NO_RB *removeNO_RB(struct NO_RB *H, int valor){
    if(valor<H->info){
        if(cor(H->esq)== BLACK && cor(H->esq->esq) == BLACK){
            H = move2EsqRED(H);
        }
        H->esq = removeNO_RB(H->esq, valor);
    }else{
        if(cor(H->esq) == RED){
            H = rotacionaDireita(H);
        }
        if(valor == H->info && (H->dir == NULL)){
            free(H);
            return NULL;
        }
        if(cor(H->dir) == BLACK && cor(H->dir->esq) == BLACK){
            H = move2DirRED(H);
        }
        if(valor == H->info){
            struct NO_RB *x = procuraMenor(H->dir);
            H->info = x->info;
            H->dir = removeMenor(H->dir);
        }else{
            H->dir = removeNO_RB(H->dir, valor);
        }
    }
    return balancear(H);
};

int consulta_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    if(raizRB == NULL){
        return 0;
    }
    struct NO_RB *atual = *raizRB;
    while(atual != NULL){
        if(valor == atual->info){
            return 1;
        }
        if(valor > atual->info){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}

int remove_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    if(consulta_arvoreLLRB(raizRB, valor)){
        struct NO_RB *H = *raizRB;
        *raizRB = removeNO_RB(H, valor);
        if(*raizRB != NULL){
            (*raizRB)->cor = BLACK;
        }
        return 1;
    }else{
        return 0;
    }
}

//=========================== Demais Fun��es

//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV2(arvoreLLRB *raizRB, dados *cad){
    int x, i = 0;
    dados *auxiliar;
    char conteudo[700];

    FILE *CSV = fopen("massaDados.csv", "r");
    if(CSV == NULL){
        printf("\n\nFalha na abertura da massa de dados!\n\n");
        system("pause");
        return 0;
    }

    while(EOF){
    fgets(conteudo, 700, CSV);
    cad->codigo = atoi(strtok(conteudo, ";"));
    strcpy(cad->nome, strtok(NULL, ";"));
    cad->idade = atoi(strtok(NULL, ";"));
    strcpy(cad->empresa, strtok(NULL, ";"));
    strcpy(cad->departamento, strtok(NULL, ";"));
    cad->salario = atof(strtok(NULL, "\n"));

    auxiliar = cad;

    if(cad->codigo > 0){
        x = insere_arvoreLLRB(raizRB, cad);
    }

    i++;
    if(i == 15000){
        fclose(CSV);
        return 1;
        }

    }
    printf("\n\tQuantidade: %d\n\n", i);
    system("pause");

}
