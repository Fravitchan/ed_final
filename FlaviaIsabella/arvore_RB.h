typedef struct cadastro_pessoa dados;

//dados csv_struct (char *var);

//fun��o que gera o menu principal.
int menu_principal();



// ========================================== Fun��es RUBRO-NEGRA
typedef struct NO_RB *arvoreLLRB;

arvoreLLRB *cria_arvLLRB();

int insere_arvoreLLRB(arvoreLLRB *raizRB, int valor);

int remove_arvoreLLRB(arvoreLLRB *raizRB, int valor);

int consulta_arvoreLLRB(arvoreLLRB *raizRB, int valor);

//Fun��es auxiliares
int cor(struct NO_RB *H);

void trocaCor(struct NO_RB *H);

struct NO_RB *rotacionaEsquerda(struct NO_RB *A);

struct NO_RB *rotacionaDireita(struct NO_RB *A);

struct NO_RB *move2EsqRED(struct NO_RB *H);

struct NO_RB *move2DirRED(struct NO_RB *H);

struct NO_RB *balancear(struct NO_RB *H);

struct NO_RB *insereNO(struct NO_RB *H, int valor, int *resp);

struct NO_RB *removeMenor(struct NO_RB *H);

struct NO_RB *procuraMenor(struct NO_RB* atual);

struct NO_RB *removeNO(struct NO *H, int valor);

//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV2(arvoreLLRB *raizRB, dados *info);

