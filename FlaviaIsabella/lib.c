#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lib.h"

#define RED 1
#define BLACK 0

//Informa��es da tabela csv
typedef struct cadastro_pessoa{
    unsigned int codigo;
    char nome[35];
    unsigned int idade;
    char empresa[35];
    char departamento[35];
    float salario;
}dados; //informa��es cadastrais


// ===== Fun��es AVL
struct NO_AVL{
    dados info;
    int alt; //inclui a altura da sub-�rvore
    struct NO_AVL *esq;
    struct NO_AVL *dir;
};

arvAVL *cria_arvAVL(){

    arvAVL *raiz = (arvAVL*) malloc(sizeof(arvAVL));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

/*
* Fun��es auxiliares para tratamento de inser��es
* e remo��es em AVL
*/

//calcula a altura de um n�
int alt_no(struct NO_AVL *no){
    if(no == NULL){
        return -1;
    }else{
        return no->alt;
    }
}

//clacula o fator de balanceamento de um n�
int fatorBalanceamento_NO_AVL(struct NO_AVL *no){
    return labs(alt_no(no->esq) - alt_no(no->dir));
} // a fun��o labs arredonda valores em m�dulo

//clacula maior valor
int maior(int x, int y){
    if(x > y){
        return(x);
    }else{
        return(y);
    }
}

//rota��o simples direita
void rotacaoLL(arvAVL *raiz){
    struct NO_AVL *no;
    no = (*raiz)->esq;
    (*raiz)->esq = no->dir;
    no->dir = *raiz;
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->esq), (*raiz)->alt + 1);
}

//rota��o simples esquerda
void rotacaoRR(arvAVL *raiz){
    struct NO_AVL *no;
    no = (*raiz)->dir;
    (*raiz)->dir = no->esq;
    no->esq = (*raiz);
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior (alt_no(no->dir), (*raiz)->alt) + 1;
    (*raiz) = no;
}

//rota��o LR
void rotacaoLR(arvAVL *raiz){
    rotacaoRR(&(*raiz)->esq);
    rotacaoLL(raiz);
}

//rota��o RL
void rotacaoRL(arvAVL *raiz){
    rotacaoLL(&(*raiz)->dir);
    rotacaoRR(raiz);
}

//inser��o
int insere_arvAVL(arvAVL *raiz, dados valor){
     //printf("1");

    int res; //pega a resposta das chamadas de fun��o

    if(*raiz == NULL){


        printf("1");

        struct NO_AVL *novo;
        novo = (struct NO_AVL*) malloc(sizeof(struct NO_AVL));
        if(novo == NULL){
         printf("2");
            return 0;
        }
        novo->info = valor;
        novo->alt = 0;
        novo->esq = NULL;
        novo->dir = NULL;
        *raiz = &novo;

        return 1;
    }
     printf("3");
    struct NO_AVL *atual = *raiz;
    if(valor.codigo < atual->info.codigo){
             printf("8");
        if((res = insere_arvAVL(&(atual->esq), valor)) == 1){
                 printf("4");
            if(fatorBalanceamento_NO_AVL(atual) >=2){
                 printf("5");
                if(valor.codigo < (*raiz)->esq->info.codigo){
                     printf("6");
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
         printf("7");
                }
            }
        }
    }else{
        if(valor.codigo > atual->info.codigo){
            if((res = insere_arvAVL(&(atual->dir), valor)) == 1){
                if(fatorBalanceamento_NO_AVL(atual) >=2){
                    if((*raiz)->dir->info.codigo < valor.codigo){
                        rotacaoRR(raiz);
                    }else{
                        rotacaoRL(raiz);
                    }
                }
            }
        }else{
            printf("Valor duplicado!\n");
            return 0;
        }
    }
    atual->alt = maior(alt_no(atual->esq), alt_no(atual->dir)) + 1;
    printf("\nres:%d\n",res);
    return res;

}

int consulta_arvoreAVL(arvAVL *raiz, dados valor){
    if(raiz == NULL){
        return 0;
    }
    struct NO_AVL *atual = *raiz;
    while(atual != NULL){
        if(valor.codigo == atual->info.codigo){
            return 1;
        }
        if(valor.codigo > atual->info.codigo){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}


int remove_arvAVL(arvAVL *raiz, dados valor){
    if(*raiz == NULL){
        return 0;
    }
    int res;
    if(valor.codigo < (*raiz)->info.codigo){
        if((res = remove_arvAVL(&(*raiz)->esq, valor)) == 1){
            if(fatorBalanceamento_NO_AVL(*raiz) >= 2){
                if(alt_no((*raiz)->dir->esq <= alt_no((*raiz)->dir->dir))){
                    rotacaoRR(raiz);
                }else{
                    rotacaoRL(raiz);
                }
            }
        }
    }
    if((*raiz)->info.codigo < valor.codigo){
        if((res = remove_arvAVL(&(*raiz)->dir, valor)) == 1){
            if(fatorBalanceamento_NO_AVL(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir <= alt_no((*raiz)->esq->esq))){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }
    if((*raiz)->info.codigo == valor.codigo){
        if(((*raiz)->esq == NULL) || (*raiz)->dir == NULL){
            struct NO_AVL *no_velho = (*raiz);
            if((*raiz)->esq != NULL){
                *raiz = (*raiz)->esq;
            }else{
                *raiz = (*raiz)->dir;
            }
            free(no_velho);
        }else{
            struct NO_AVL *temp = procuramenor((*raiz)->dir);
            (*raiz)->info = temp->info;
            remove_arvAVL((*raiz)->dir, (*raiz)->info);
            if(fatorBalanceamento_NO_AVL(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir) <= alt_no((*raiz)->esq->esq)){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
        if(*raiz != NULL){
            (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
        }
        return 1;
    }
    if(*raiz != NULL){
        (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    }
    return res;
}

//fun��o auxiliar - procura s� mais a esquerda
struct NO_AVL *procuramenor(struct NO_AVL *atual){
    struct NO_AVL *no1 = atual;
    struct NO_AVL *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
};


//=========================== Demais Fun��es

//fun��o que gera o menu principal.
int menu_principal(){
    int escolha;

    printf("\n\n\t\t====== MENU PRINCIPAL ======\n\n");
    printf("\t|1 -> Realizar operacoes com a arvore AVL.");
    printf("\n\t|2 -> Realizar operacoes com a arvore RUBRO NEGRA.");
    printf("\n\t|3 -> Salvar e sair.");
    printf("\n\n\tInforme o numero da opcao desejada ---> ");
    scanf("%d", &escolha);
    system("cls");
    return escolha;

}

//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV1(arvAVL *raiz){
    int x, i = 0;
    dados info;
    char conteudo[700];


    FILE *CSV = fopen("massaDados.csv", "r");

    if(CSV == NULL){
        printf("\n\nFalha na abertura da massa de dados!\n\n");
        system("pause");
        return 0;
    }else{
         printf("\tLeitura realizada com sucesso!\n");
    }


    while(EOF){
    fgets(conteudo, 700, CSV);
    info.codigo = atoi(strtok(conteudo, ";"));
    strcpy(info.nome, strtok(NULL, ";"));
    info.idade = atoi(strtok(NULL, ";"));
    strcpy(info.empresa, strtok(NULL, ";"));
    strcpy(info.departamento, strtok(NULL, ";"));
    info.salario = atof(strtok(NULL, "\n"));


    //auxiliar = info;

    if(info.codigo > 0){
        x = insere_arvAVL(raiz, info);


    }

    i++;
    if(i == 15000){

        fclose(CSV);
        return 1;
        }

    }


    printf("\n\tQuantidade: %d\n\n", i);
    system("pause");


    fclose(CSV);
    return 1;


}




