typedef struct cadastro_pessoa dados;

//dados csv_struct (char *var);

//fun��o que gera o menu principal.
int menu_principal();


// ========================================== Fun��es AVL
typedef struct NO_AVL* arvAVL;

arvAVL *cria_arvAVL();

//inser��o
int insere_arvAVL(arvAVL *raiz, dados valor);

//fun��o respons�vel pela busca do n� a ser removido
int remove_arvAVL(arvAVL *raiz, dados valor);

//fun��o respons�vel por tratar a remo��o de um n� com dois filhos
struct NO_AVL *procuramenor(struct NO_AVL *atual);

//calcula a altura de um n�
int alt_no(struct NO_AVL *no);

//clacula o fator de balanceamento de um n�
int fatorBalanceamento_NO_AVL(struct NO_AVL *no);

//clacula maior valor
int maior(int x, int y);

//rota��o simples direita
void rotacaoLL(arvAVL *raiz);

//rota��o simples esquerda
void rotacaoRR(arvAVL *raiz);

//rota��o LR
void rotacaoLR(arvAVL *raiz);

//rota��o RL
void rotacaoRL(arvAVL *raiz);



//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV1(arvAVL *raiz);








