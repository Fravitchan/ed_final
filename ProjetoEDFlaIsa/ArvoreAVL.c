#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ArvoreAVL.h"

//Informacoes tabela CSV


//Função AVL
typedef struct NO
{
    dados info;
    int alt; //inclui a altura da sub-�rvore
    struct NO *esq;
    struct NO *dir;
};

ArvAvl *cria_ArvAvl()
{
    ArvAvl *raiz = (ArvAvl*) malloc(sizeof(ArvAvl));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

void libera_ArvAvl(ArvAvl *raiz)
{
    if(raiz == NULL){
        return;
    }
    libera_NO(*raiz);
    printf("\n\n\tArvore Limpa\n");
    free(raiz);
}

void libera_NO(struct NO *no)
{
    if(no == NULL){
        return;
    }
    libera_NO(no->esq);
    libera_NO(no->dir);
    free(no);
    no = NULL;
}

int vazia_ArvAvl(ArvAvl *raiz){
    if(raiz == NULL)
    {
        return 1;
    }
    if(*raiz == NULL)
    {
        return 1;
    }
    return 0;
}
struct NO *menor(struct NO *atual){
    struct NO *no = atual;
    struct NO *no2 = atual->esq;
    while(no2 != NULL){
        no = no2;
        no2 = no2->esq;
    }
    return no;
};

int alt_no(struct NO *no)
{
    if(no == NULL){
        return -1;
    }else{
        return no->alt;
    }
}

int fatorBalanceamento_NO(struct NO *no)
{
    return labs(alt_no(no->esq) - alt_no(no->dir));
}

int maior (int x, int y){
    if(x > y){
        return(x);
    }else{
        return(y);
    }
}

//ROTACAO

void rotacaoRR(ArvAvl *raiz)
{
    struct NO *no;
    no = (*raiz)->dir;
    (*raiz)->dir = no->esq;
    no->esq = (*raiz);
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->dir), (*raiz)->alt )+ 1;
    (*raiz) = no;
}

void rotacaoLL(ArvAvl *raiz)
{
    struct NO *no;
    no = (*raiz)->esq;
    (*raiz)->esq = no->dir;
    no->dir = *raiz;
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->esq), (*raiz)->alt + 1);
    *raiz = no;
}

void rotacaoLR(ArvAvl *raiz)
{
    rotacaoRR(&(*raiz)->esq);
    rotacaoLL(raiz);
}

void rotacaoRL(ArvAvl *raiz)
{
    rotacaoLL(&(*raiz)->dir);
    rotacaoRR(raiz);
}


int insere_ArvAvl(ArvAvl *raiz, dados *info)
{

    int res;
    if(*raiz == NULL){ //arvore vazia ou n� folha

        struct NO *novo;
        novo = (struct NO*)malloc(sizeof(struct NO));
        if(novo == NULL){
            return 0;
        }
        novo->info = (*info);
        novo->alt = 0;
        novo->esq = NULL;
        novo->dir = NULL;
        *raiz = novo;
        return 1;

    }

    struct NO *atual = *raiz;
    if(info->cod < atual->info.cod){
        if((res = insere_ArvAvl(&(atual->esq), info)) == 1){
            if(fatorBalanceamento_NO(atual) >= 2){
                if(info->cod < (*raiz)->esq->info.cod){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }else{
        if(info->cod > atual->info.cod){
            if((res = insere_ArvAvl(&(atual->dir), info)) == 1){
                if(fatorBalanceamento_NO(atual) >= 2){
                    if((*raiz)->dir->info.cod < info->cod){
                        rotacaoRR(raiz);
                    }else{
                        rotacaoRL(raiz);
                    }
                }
            }
        }else{
            printf("Valor duplicado!\n");
            return 0;
        }
    }
    atual->alt  = maior(alt_no(atual->esq), alt_no(atual->dir)) + 1;
    return res;
}

int leituraCSV1(ArvAvl *raiz, struct cadastroPessoa *info){
    int i=0, x;
    struct Pessoa *aux;
    char texto[20000];
    FILE *arquivo = fopen("massaDados.csv", "r");

    if(arquivo == NULL){
        printf("\nFalha na abertura do arquivo!\n\n");
        system("pause");
        return 0;
    }

    while(EOF){
        fgets(texto, 20000, arquivo);
        info->cod = atoi(strtok(texto, ";"));
        strcpy(info->nome, strtok(NULL, ";"));
        info->idade = atoi(strtok(NULL, ";"));
        strcpy(info->empresa, strtok(NULL, ";"));
        strcpy(info->departamento, strtok(NULL, ";"));
        info->salario = atof(strtok(NULL, "\n"));
        i++;
        aux = info;

        x = insere_ArvAvl(raiz, info);
        if(i == 14999){
            fclose(arquivo);
            return 1;
        }
    }
    printf("\nQuantidade: %d\n\n", i);
    system("pause");

    fclose(arquivo);
    return 1;
}


int consulta_ArvAvl(ArvAvl *raiz, int valor){

    if(raiz == NULL){
        return 0;
    }

    struct NO *atual = *raiz;


    while(atual){

        if(valor == atual->info.cod){
            printf("\n\t\tNO PAI\n");
            printf("\tID: %d",atual->info.cod);
            printf("\n\tNome: %s\n", atual->info.nome);



            if(atual->esq != NULL){
                printf("\n\t\tFILHO ESQUERDO");
                printf("\n\tID: %d\n", atual->esq->info.cod);
                printf("\tNome: %s",atual->esq->info.nome);
                printf("\n\tIdade: %d",atual->esq->info.idade);
            }else{
                printf("\n\tNao possui filho esquerdo\n");
            }
            if(atual->dir != NULL){
                printf("\n\t\tFILHO DIREITO");
                printf("\n\tID: %d\n", atual->dir->info.cod);
                printf("\tNome: %s",atual->dir->info.nome);
                printf("\n\tIdade: %d",atual->dir->info.idade);
            }else{
                printf("\n\tNao possui filho direito\n");
            }

            if(atual->dir == NULL && atual->esq == NULL)
            {
                printf("\n\tEsse NO e uma folha\n");
            }

            if( atual->info.cod == 53817 ||  //Remocao filho direito
                atual->info.cod == 37792 ||
                atual->info.cod == 84858 ||
                atual->info.cod == 81073 ||
                atual->info.cod == 16450   )
            {
                if(atual->dir != NULL)
                    {
                        remove_ArvAvl(raiz, atual->dir->info.cod);
                    }else
                    {
                        printf("\n\tNAO possui filho para ser removido");
                    }
            }
             if( atual->info.cod == 91648 ||  //Remocao filho esquerdo
                 atual->info.cod == 92281 ||
                 atual->info.cod == 8838 ||
                 atual->info.cod == 77944 ||
                 atual->info.cod == 26875  )
            {
                if(atual->dir != NULL)
                    {
                        remove_ArvAvl(raiz, atual->esq->info.cod);
                    }else
                    {
                        printf("\n\tNAO possui filho para ser removido");
                    }
            }
        }

        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }


    }
}


int remove_ArvAvl(ArvAvl *raiz, int valor)
{
    if(raiz == NULL)
    {
        return 0;
    }
    int res;
    if(valor < (*raiz)->info.cod)
    {
        if((res = remove_ArvAvl(&(*raiz)->esq, valor)) == 1)
        {
            if(fatorBalanceamento_NO(*raiz) >= 2)
            {
                if(alt_no((*raiz)->dir->esq) <= alt_no((*raiz)->dir->dir))
                {
                    rotacaoRR(raiz);
                }

                else
                {
                    rotacaoRL(raiz);
                }
            }
        }
    }
    if((*raiz)->info.cod < valor){
        if((res = remove_ArvAvl(&(*raiz)->dir, valor)) == 1){
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir) <= alt_no((*raiz)->esq->esq)){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }
    if((*raiz)->info.cod == valor)
    {
        if(((*raiz)->esq == NULL || (*raiz)->dir == NULL))
        {
            struct NO *noVelho = (*raiz);
            if((*raiz)->esq != NULL){
                *raiz = (*raiz)->esq;
            }else
            {
                *raiz = (*raiz)->dir;
            }
            free(noVelho);
        }
        else
        {
            struct NO* temp = menor((*raiz)->dir);
            (*raiz)->info = temp->info;
            remove_ArvAvl(&(*raiz)->dir, (*raiz)->info.cod);
            if(fatorBalanceamento_NO(*raiz) >= 2)
            {
                if(alt_no((*raiz)->esq->dir) <= alt_no((*raiz)->esq->esq))
                {
                    rotacaoLL(raiz);
                }
                else
                {
                    rotacaoLR(raiz);
                }
            }
        }
        return 1;
    }
    return res;

}

int mostraPosExclusao_ArvAvl(ArvAvl *raiz, int valor){
  if(raiz == NULL){
        return 0;
    }

    struct NO *atual = *raiz;


    while(atual){

        if(valor == atual->info.cod){
            printf("\n\t\tNO PAI\n");
            printf("\tID: %d",atual->info.cod);
            printf("\n\tNome: %s\n", atual->info.nome);



            if(atual->esq != NULL){
                printf("\n\t\tFILHO ESQUERDO");
                printf("\n\tID: %d\n", atual->esq->info.cod);
                printf("\tNome: %s",atual->esq->info.nome);
                printf("\n\tIdade: %d",atual->esq->info.idade);
            }else{
                printf("\n\tNao possui filho esquerdo\n");
            }
            if(atual->dir != NULL){
                printf("\n\t\tFILHO DIREITO");
                printf("\n\tID: %d\n", atual->dir->info.cod);
                printf("\tNome: %s",atual->dir->info.nome);
                printf("\n\tIdade: %d",atual->dir->info.idade);
            }else{
                printf("\n\tNao possui filho direito\n");
            }

            if(atual->dir == NULL && atual->esq == NULL)
            {
                printf("\n\tEsse NO e uma folha\n");
            }


        }

        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }


    }
}


