typedef struct NO *ArvAvl;
typedef struct cadastroPessoa
{
    int cod;
    char nome[50];
    int idade;
    char empresa[50];
    char departamento[50];
    float salario;
}dados;


ArvAvl *cria_ArvAvl();

void libera_ArvAvl(ArvAvl *raiz);

int vazia_ArvAvl(ArvAvl *raiz);

int leituraCSV1(ArvAvl *raiz, struct cadastroPessoa *info);

int consulta_ArvAvl(ArvAvl *raiz, int valor);

int mostraPosExclusao_ArvAvl(ArvAvl *raiz, int valor);
