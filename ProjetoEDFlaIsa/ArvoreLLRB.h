
typedef struct NO *ArvLLRB;

typedef struct cadastroPessoa2
{
    int cod;
    char nome[50];
    int idade;
    char empresa[50];
    char departamento[50];
    float salario;
}dados2;


ArvLLRB *cria_ArvLLRB();

void libera_ArvLLRB(ArvLLRB *raiz);

int vazia_ArvLLRB(ArvLLRB *raiz);

int leituraCSVArvLLRB(ArvLLRB *raiz, struct cadastroPessoa2 *info);

int consulta_ArvLLRB(ArvLLRB *raiz, int valor);

int mostraPosExclusao_ArvAvl(ArvLLRB *raiz, int valor);

