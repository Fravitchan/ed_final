typedef struct cadastro_pessoa cadInfo;

//cadInfo csv_struct (char *var);

//fun��o que gera o menu principal.
int menu_principal();


// ========================================== Fun��es AVL
typedef struct NO* arvAVL;

arvAVL *cria_arvAVL();

//inser��o
int insere_arvAVL(arvAVL *raiz, int valor);

//fun��o respons�vel pela busca do n� a ser removido
int remove_arvAVL(arvAVL *raiz, int valor);

//fun��o respons�vel por tratar a remo��o de um n� com dois filhos
struct NO *procuramenor(struct NO *atual);

//calcula a altura de um n�
int alt_no(struct NO *no);

//clacula o fator de balanceamento de um n�
int fatorBalanceamento_NO(struct NO *no);

//clacula maior valor
int maior(int x, int y);

//rota��o simples direita
void rotacaoLL(arvAVL *raiz);

//rota��o simples esquerda
void rotacaoRR(arvAVL *raiz);

//rota��o LR
void rotacaoLR(arvAVL *raiz);

//rota��o RL
void rotacaoRL(arvAVL *raiz);





// ========================================== Fun��es RUBRO-NEGRA
typedef struct NOb *arvoreLLRB;

arvoreLLRB *cria_arvLLRB();

int insere_arvoreLLRB(arvoreLLRB *raizRB, int valor);

int remove_arvoreLLRB(arvoreLLRB *raizRB, int valor);

int consulta_arvoreLLRB(arvoreLLRB *raizRB, int valor);

//Fun��es auxiliares
int cor(struct NOb *H);

void trocaCor(struct NOb *H);

struct NOb *rotacionaEsquerda(struct NOb *A);

struct NOb *rotacionaDireita(struct NOb *A);

struct NOb *move2EsqRED(struct NOb *H);

struct NOb *move2DirRED(struct NOb *H);

struct NOb *balancear(struct NOb *H);

struct NOb *insereNO(struct NOb *H, int valor, int *resp);

struct NOb *removeMenor(struct NOb *H);

struct NOb *procuraMenor(struct NO* atual);

struct NOb *removeNO(struct NO *H, int valor);


//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV1(arvAVL *raiz, cadInfo *cad);

int leituraCSV2(arvoreLLRB *raizRB, cadInfo *cad);







