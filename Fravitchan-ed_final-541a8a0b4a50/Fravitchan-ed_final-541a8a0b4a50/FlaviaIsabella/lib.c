#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lib.h"

#define RED 1
#define BLACK 0

//Informa��es da tabela csv
typedef struct cadastro_pessoa{
    unsigned int codigo;
    char nome[35];
    unsigned int idade;
    char empresa[35];
    char departamento[35];
    float salario;
}cadInfo; //informa��es cadastrais


// ===== Fun��es AVL
struct NO{
    int info;
    int alt; //inclui a altura da sub-�rvore
    struct NO *esq;
    struct NO *dir;
};

arvAVL *cria_arvAVL(){

    arvAVL *raiz = (arvAVL*) malloc(sizeof(arvAVL));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

/*
* Fun��es auxiliares para tratamento de inser��es
* e remo��es em AVL
*/

//calcula a altura de um n�
int alt_no(struct NO *no){
    if(no == NULL){
        return -1;
    }else{
        return no->alt;
    }
}

//clacula o fator de balanceamento de um n�
int fatorBalanceamento_NO(struct NO *no){
    return labs(alt_no(no->esq) - alt_no(no->dir));
} // a fun��o labs arredonda valores em m�dulo

//clacula maior valor
int maior(int x, int y){
    if(x > y){
        return(x);
    }else{
        return(y);
    }
}

//rota��o simples direita
void rotacaoLL(arvAVL *raiz){
    struct NO *no;
    no = (*raiz)->esq;
    (*raiz)->esq = no->dir;
    no->dir = *raiz;
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->esq), (*raiz)->alt + 1);
}

//rota��o simples esquerda
void rotacaoRR(arvAVL *raiz){
    struct NO *no;
    no = (*raiz)->dir;
    (*raiz)->dir = no->esq;
    no->esq = (*raiz);
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior (alt_no(no->dir), (*raiz)->alt) + 1;
    (*raiz) = no;
}

//rota��o LR
void rotacaoLR(arvAVL *raiz){
    rotacaoRR(&(*raiz)->esq);
    rotacaoLL(raiz);
}

//rota��o RL
void rotacaoRL(arvAVL *raiz){
    rotacaoLL(&(*raiz)->dir);
    rotacaoRR(raiz);
}

//inser��o
int insere_arvAVL(arvAVL *raiz, int valor){
    int res; //pega a resposta das chamadas de fun��o
    if(*raiz == NULL){
        struct NO *novo;
        novo = (struct NO*) malloc(sizeof(struct NO));
        if(novo == NULL){
            return 0;
        }
        novo->info = valor;
        novo->alt = 0;
        novo->esq = NULL;
        novo->dir = NULL;
        *raiz = novo;
        return 1;
    }
    struct NO *atual = *raiz;
    if(valor < atual->info){
        if((res = insere_arvAVL(&(atual->esq), valor)) == 1){
            if(fatorBalanceamento_NO(atual) >=2){
                if(valor < (*raiz)->esq->info){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }else{
        if(valor > atual->info){
            if((res = insere_arvAVL(&(atual->dir), valor)) == 1){
                if(fatorBalanceamento_NO(atual) >=2){
                    if((*raiz)->dir->info < valor){
                        rotacaoRR(raiz);
                    }else{
                        rotacaoRL(raiz);
                    }
                }
            }
        }else{
            printf("Valor duplicado!\n");
            return 0;
        }
    }
    atual->alt = maior(alt_no(atual->esq), alt_no(atual->dir)) + 1;
    return res;

}


int remove_arvAVL(arvAVL *raiz, int valor){
    if(*raiz == NULL){
        return 0;
    }
    int res;
    if(valor < (*raiz)->info){
        if((res = remove_arvAVL(&(*raiz)->esq, valor)) == 1){
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->dir->esq <= alt_no((*raiz)->dir->dir))){
                    rotacaoRR(raiz);
                }else{
                    rotacaoRL(raiz);
                }
            }
        }
    }
    if((*raiz)->info < valor){
        if((res = remove_arvAVL(&(*raiz)->dir, valor)) == 1){
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir <= alt_no((*raiz)->esq->esq))){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }
    if((*raiz)->info == valor){
        if(((*raiz)->esq == NULL) || (*raiz)->dir == NULL){
            struct NO *no_velho = (*raiz);
            if((*raiz)->esq != NULL){
                *raiz = (*raiz)->esq;
            }else{
                *raiz = (*raiz)->dir;
            }
            free(no_velho);
        }else{
            struct NO *temp = procuramenor((*raiz)->dir);
            (*raiz)->info = temp->info;
            remove_arvAVL((*raiz)->dir, (*raiz)->info);
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir) <= alt_no((*raiz)->esq->esq)){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
        if(*raiz != NULL){
            (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
        }
        return 1;
    }
    if(*raiz != NULL){
        (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    }
    return res;
}

//fun��o auxiliar - procura s� mais a esquerda
struct NO *procuramenor(struct NO *atual){
    struct NO *no1 = atual;
    struct NO *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
};


// ===== Fun��es RB
struct NOb{
    int info;
    struct NOb *esq;
    struct NOb *dir;
    int cor;
};

arvoreLLRB *cria_arvLLRB(){

    arvoreLLRB *raizRB = (arvoreLLRB*) malloc(sizeof(arvoreLLRB));
    if(raizRB != NULL){
        *raizRB = NULL;
    }
    return raizRB;
}


//Fun��es auxiliares
int cor(struct NOb *H){
    if(H == NULL){
        return BLACK;
    }else{
        return H->cor;
    }
}

void trocaCor(struct NOb *H){
    H->cor = !H->esq->cor;
    if(H->esq != NULL){
        H->esq->cor = !H->esq->cor;
    }
    if(H->dir != NULL){
        H->dir->cor = !H->dir->cor;
    }
}

struct NOb *rotacionaEsquerda(struct NOb *A){
    struct NOb *B = A->dir;
    A->dir = B->esq;
    B->esq = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NOb *rotacionaDireita(struct NOb *A){
    struct NOb *B = A->esq;
    A->esq = B->dir;
    B->dir = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
};

struct NOb *move2EsqRED(struct NOb *H){
    trocaCor(H);
    if(cor(H->dir->esq) == RED){
        H->dir = rotacionaDireita(H->dir);
        H = rotacionaEsquerda(H);
        trocaCor(H);
    }
    return H;
};

struct NOb *move2DirRED(struct NOb *H){
    trocaCor(H);
    if(cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
        trocaCor(H);
    }
    return H;
};

struct NOb *balancear(struct NOb *H){
    if(cor(H->dir) == RED){
        H = rotacionaEsquerda(H);
    }
    if(H->esq != NULL && cor(H->dir) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};



struct NOb *insereNO(struct NOb *H, int valor, int *resp){
    if(H == NULL){
        struct NOb *novo;
        novo = (struct NOb*) malloc(sizeof(struct NOb));
        if(novo == NULL){
            *resp = 0;
            return NULL;
        }
        novo->info = valor;
        novo->cor = RED;
        novo->dir = NULL;
        novo->esq = NULL;
        *resp = 1;
        return novo;
    }
    if(valor == H->info){
        *resp = 0;
    }else{
        if(valor < H->info){
            H->esq = insereNO(H->esq, valor, resp);
        }else{
            H->dir = insereNO(H->dir, valor, resp);
        }
    }
    if(cor(H->dir) == RED && cor(H->esq) == BLACK){
        H = rotacionaEsquerda(H);
    }
    if(cor(H->esq) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
};

int insere_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    int resp;
    //fun��o respons�vel pela busca do local de inser��o do n�
    *raizRB = insereNO(*raizRB, valor, &resp);
    if((*raizRB) != NULL){
        (*raizRB)->cor = BLACK;
    }
    return resp;
}
struct NOb *removeMenor(struct NOb *H){
    if(H->esq == NULL){
        free(H);
        return NULL;
    }
    if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
        H = move2EsqRED(H);
    }
    H->esq = removeMenor(H->esq);
    return balancear(H);
};

struct NOb *procuraMenor(struct NO* atual){
    struct NOb *no1 = atual;
    struct NOb *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
};

struct NOb *removeNOb(struct NOb *H, int valor){
    if(valor<H->info){
        if(cor(H->esq)== BLACK && cor(H->esq->esq) == BLACK){
            H = move2EsqRED(H);
        }
        H->esq = removeNOb(H->esq, valor);
    }else{
        if(cor(H->esq) == RED){
            H = rotacionaDireita(H);
        }
        if(valor == H->info && (H->dir == NULL)){
            free(H);
            return NULL;
        }
        if(cor(H->dir) == BLACK && cor(H->dir->esq) == BLACK){
            H = move2DirRED(H);
        }
        if(valor == H->info){
            struct NOb *x = procuraMenor(H->dir);
            H->info = x->info;
            H->dir = removeMenor(H->dir);
        }else{
            H->dir = removeNOb(H->dir, valor);
        }
    }
    return balancear(H);
};

int consulta_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    if(raizRB == NULL){
        return 0;
    }
    struct NOb *atual = *raizRB;
    while(atual != NULL){
        if(valor == atual->info){
            return 1;
        }
        if(valor > atual->info){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}

int remove_arvoreLLRB(arvoreLLRB *raizRB, int valor){
    if(consulta_arvoreLLRB(raizRB, valor)){
        struct NOb *H = *raizRB;
        *raizRB = removeNOb(H, valor);
        if(*raizRB != NULL){
            (*raizRB)->cor = BLACK;
        }
        return 1;
    }else{
        return 0;
    }
}

//=========================== Demais Fun��es

//fun��o que gera o menu principal.
int menu_principal(){
    int escolha;

    printf("\n\n\t\t====== MENU PRINCIPAL ======\n\n");
    printf("\t|1 -> Realizar operacoes com a arvore AVL.");
    printf("\n\t|2 -> Realizar operacoes com a arvore RUBRO NEGRA.");
    printf("\n\t|3 -> Salvar e sair.");
    printf("\n\n\tInforme o numero da opcao desejada ---> ");
    scanf("%d", &escolha);
    system("cls");
    return escolha;

}

//Fun��o que l� o arquivo de massa de dados (CSV).
int leituraCSV1(arvAVL *raiz, cadInfo *cad){
    int x, i = 0;
    cadInfo *auxiliar;
    char conteudo[700];

    FILE *CSV = fopen("massaDados.csv", "r");
    if(CSV == NULL){
        printf("\n\nFalha na abertura da massa de dados!\n\n");
        system("pause");
        return 0;
    }

    while(EOF){
    fgets(conteudo, 700, CSV);
    cad->codigo = atoi(strtok(conteudo, ";"));
    strcpy(cad->nome, strtok(NULL, ";"));
    cad->idade = atoi(strtok(NULL, ";"));
    strcpy(cad->empresa, strtok(NULL, ";"));
    strcpy(cad->departamento, strtok(NULL, ";"));
    cad->salario = atof(strtok(NULL, "\n"));

    auxiliar = cad;

    if(cad->codigo > 0){
        x = insere_arvAVL(raiz, cad);
    }

    i++;
    if(i == 15000){
        fclose(CSV);
        return 1;
        }

    }
    printf("\n\tQuantidade: %d\n\n", i);
    system("pause");


    fclose(CSV);
    return 1;


}

int leituraCSV2(arvoreLLRB *raizRB, cadInfo *cad){
    int x, i = 0;
    cadInfo *auxiliar;
    char conteudo[700];

    FILE *CSV = fopen("massaDados.csv", "r");
    if(CSV == NULL){
        printf("\n\nFalha na abertura da massa de dados!\n\n");
        system("pause");
        return 0;
    }

    while(EOF){
    fgets(conteudo, 700, CSV);
    cad->codigo = atoi(strtok(conteudo, ";"));
    strcpy(cad->nome, strtok(NULL, ";"));
    cad->idade = atoi(strtok(NULL, ";"));
    strcpy(cad->empresa, strtok(NULL, ";"));
    strcpy(cad->departamento, strtok(NULL, ";"));
    cad->salario = atof(strtok(NULL, "\n"));

    auxiliar = cad;

    if(cad->codigo > 0){
        x = insere_arvoreLLRB(raizRB, cad);
    }

    i++;
    if(i == 15000){
        fclose(CSV);
        return 1;
        }

    }
    printf("\n\tQuantidade: %d\n\n", i);
    system("pause");


    fclose(CSV);
    return 1;


}




